1- Télécharger le code source, et s'assurer que Python est télécharger sur notre ordinateur.

2- Vérifier que tout les fichiers sont dans le même répertoire.

3- Lancer le fichier main.py uniquement.

4- Lancer soit le mode easy soit le mode hard.

5- Une liste de mots de de passe sont affichés avec la distance qui les sépare du mot de passe à chercher.

6- Proposer des mots de passe jusque trouver le bon, le jeu se fermera trois secondes après avoir gagner après avoir afficher le score.

/!\ Pour information, la distance écrite consiste en le nombre de caractères qui divergent entre le mot de passe proposé et le mot de passe attendu.

Une technique possible pour gagner le plus rapidement possible est de parcourir chacun des caractères du mot de passe avec la distance la plus courte et voir si la distance change : si elle change pas le caractères est faut et il faut écrire soit celui d'avant soit celui d'après.