﻿Les pré-requis de lancement:
* avoir un logiciel ou application pour lancer python
* avoir le code pour jouer
* savoir comment marche la logique du k-voisin






Fonctionnement du projet:
* chaine_similaire(liste_chaines) qui génère une nouvelle chaîne similaire à celle choisie aléatoirement dans la liste d'entrée liste_chaines.
* Voici comment cela fonctionne étape par étape :
* Importe le module aléatoire.
* Définit la fonction chaine_similaire qui prend liste_chaines en entrée.
* Sélectionne aléatoirement une chaîne chaine_choisie dans la liste d'entrée liste_chaines.
* Initialise une chaîne vide nouvelle_chaine pour la nouvelle chaîne similaire.
* Parcourt chaque caractère (caractère) de la chaîne choisie.
* Vérifie si un nombre généré aléatoirement est inférieur à 0,5.
* Si c'est vrai, il calcule un nouveau caractère en ajoutant un entier aléatoire compris entre -1 et 1 à la valeur ASCII du caractère actuel.
* Si faux, il conserve le caractère actuel tel quel.
* Renvoie la chaîne similaire nouvellement générée.
* Fournit un commentaire en français se traduisant par : "voici le programme qui a été utilisé pour créer un mot de passe aléatoire assez proche de celui dans passwords.toml`.