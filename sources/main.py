from tkinter import *
import programme

def main():
    root = Tk()
    app = programme.HackInterface(root)
    root.mainloop()

if __name__ == "__main__":
    main()
