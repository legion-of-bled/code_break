import random


def chaine_similaire(liste_chaines):

  chaine_choisie = random.choice(liste_chaines)
  #print(chaine_choisie)

  nouvelle_chaine = ""
  for caractere in chaine_choisie:
    if random.random() < 0.5:
      nouvelle_chaine += chr(
          ord(caractere) + random.randint(-1, 1)
      )  #Plus ces deux valeurs sont proches plus le mot de passe ressemble à un de la liste, et inversement

    else:
      nouvelle_chaine += caractere

  return nouvelle_chaine


"""hnaya c le programme li kan sta3mlo pour créer un mot de passe 
aléatoire assez proche de hadom dans passwords.toml"""
