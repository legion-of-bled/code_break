import tkinter as tk
import time
from random import randint
import generator


class HackInterface:
  score = 0

  def __init__(self, root):
    self.root = root
    self.root.title("HACK Interface")
    self.root.attributes('-fullscreen', True)
    self.root.configure(bg="black")

    self.title_label = tk.Label(self.root,
                                text="HACK INTERFACE",
                                fg="green",
                                bg="black",
                                font=("Helvetica", 40, "bold"))
    self.title_label.place(relx=0.5, rely=0.25, anchor=tk.CENTER)

    self.emoji_left = tk.Label(self.root,
                               text="👾",
                               fg="light green",
                               bg="black",
                               font=("Arial", 30))
    self.emoji_left.place(relx=0.3, rely=0.5, anchor=tk.CENTER)

    self.emoji_right = tk.Label(self.root,
                                text="👾",
                                fg="light green",
                                bg="black",
                                font=("Arial", 30))
    self.emoji_right.place(relx=0.7, rely=0.5, anchor=tk.CENTER)

    self.welcome_label = tk.Label(self.root,
                                  text="Bienvenue dans l'interface de HACK !",
                                  fg="light green",
                                  bg="black",
                                  font=("Helvetica", 20))
    self.welcome_label.place(relx=0.5, rely=0.35, anchor=tk.CENTER)

    self.start_button_easy = tk.Button(self.root,
                                       text="Easy",
                                       command=self.start_hacking_easy,
                                       font=("Helvetica", 20),
                                       bg="green",
                                       fg="white",
                                       padx=20,
                                       pady=10,
                                       relief=tk.GROOVE)
    self.start_button_easy.place(relx=0.45, rely=0.5, anchor=tk.CENTER)

    self.start_button_hard = tk.Button(self.root,
                                       text="Hard",
                                       command=self.start_hacking_hard,
                                       font=("Helvetica", 20),
                                       bg="green",
                                       fg="white",
                                       padx=20,
                                       pady=10,
                                       relief=tk.GROOVE)
    self.start_button_hard.place(relx=0.55, rely=0.5, anchor=tk.CENTER)

    hacker_numbers = "0123456789"
    while getattr(self, "animate_hacker_running", True):
      for num in hacker_numbers:
        self.hacker_numbers_left = tk.Label(self.root,
                                            text=num,
                                            fg="green",
                                            bg="black",
                                            font=("Courier", 40))
        self.hacker_numbers_left.place(relx=0.25, rely=0.25, anchor=tk.CENTER)
        self.hacker_numbers_right = tk.Label(self.root,
                                             text=num,
                                             fg="green",
                                             bg="black",
                                             font=("Courier", 40))
        self.hacker_numbers_right.place(relx=0.75, rely=0.25, anchor=tk.CENTER)
        self.root.update()
        time.sleep(0.05)
        self.hacker_numbers_left.destroy()
        self.hacker_numbers_right.destroy()

    self.center_window()

  def center_window(self):
    self.root.update_idletasks()
    screen_width = self.root.winfo_screenwidth()
    screen_height = self.root.winfo_screenheight()
    window_width = self.root.winfo_width()
    window_height = self.root.winfo_height()
    x = (screen_width - window_width) // 2
    y = (screen_height - window_height) // 2
    self.root.geometry('+{}+{}'.format(x, y))

  def start_hacking_easy(self):
    self.title_label.destroy()
    self.start_button_easy.destroy()
    self.start_button_hard.destroy()
    self.animate_hacker_running = False

    self.text_area = tk.Text(self.root,
                             bg="black",
                             fg="light green",
                             font=("Helvetica", 18))
    self.text_area.pack(fill=tk.BOTH, expand=True)
    self.text_area.insert(
        tk.END,
        "Regle : Il s'agit de décrypter le mot de passe à partir d'une liste de mot de passe qui s'apparente au mot de passe à décrypter. Une distance est liée à chaque mot de passe , plus cette distance est petite, plus le mot de passe est proche du mot de passe à décrypter \n\n"
    )
    self.text_area.insert(tk.END,
                          "Initialisation du piratage (Easy)... 👾👾👾\n\n")
    self.text_area.bind("<Escape>", self.close_interface)

    self.start_hacking(19, 5)

  def start_hacking_hard(self):
    self.title_label.destroy()
    self.start_button_easy.destroy()
    self.start_button_hard.destroy()
    self.animate_hacker_running = False

    self.text_area = tk.Text(self.root,
                             bg="black",
                             fg="light green",
                             font=("Helvetica", 18))
    self.text_area.pack(fill=tk.BOTH, expand=True)
    self.text_area.insert(
        tk.END,
        "Regle : Il s'agit de décrypter le mot de passe à partir d'une liste de mot de passe qui s'apparente au mot de passe à décrypter. Une distance est liée à chaque mot de passe , plus cette distance est petite, plus le mot de passe est proche du mot de passe à décrypter \n\n"
    )
    self.text_area.insert(tk.END,
                          "Initialisation du piratage (Hard)... 👾👾👾\n\n")
    self.text_area.bind("<Escape>", self.close_interface)

    self.start_hacking(29, 8)

  def start_hacking(self, max, l):
    max_length = randint(6, max)
    with open('passwords.toml', 'r') as f:
      password_list = f.read().splitlines()
      password_list = password_list[max_length - l:max_length]
    self.target_password = generator.chaine_similaire(password_list)
    self.text_area.insert(tk.END, "Liste des mots de passe avec distances:\n")
    for password in password_list:
      distance = self.calculate_distance(self.target_password, password)
      self.text_area.insert(
          tk.END, f"Mot de passe: {password}, Distance: {distance}\n")

    self.input_frame = tk.Frame(self.root, bg="light green", padx=20, pady=10)
    self.input_frame.pack(side=tk.BOTTOM, fill=tk.X)
    self.input_entry = tk.Entry(self.input_frame,
                                bg="light green",
                                fg="white",
                                font=("Helvetica", 16))
    self.input_entry.pack(fill=tk.X)

    # Bouton pour soumettre les propositions
    self.submit_button = tk.Button(self.input_frame,
                                   text="Soumettre",
                                   command=self.submit_password,
                                   font=("Helvetica", 16),
                                   bg="green",
                                   fg="white",
                                   padx=10,
                                   pady=5)
    self.submit_button.pack()

  def submit_password(self):
    self.score += 1
    proposition = self.input_entry.get().strip()
    self.text_area.insert(
        tk.END,
        f"Proposition : {proposition}, Distance : {self.calculate_distance(proposition, self.target_password)}\n"
    )
    self.input_entry.delete(0, tk.END)
    if self.calculate_distance(proposition, self.target_password) == 0:
      self.text_area.tag_configure("center", justify='center')
      self.text_area.tag_configure("large", font=("Helvetica", 24))
      self.text_area.tag_configure("yellow", foreground="yellow")
      self.text_area.insert(
          tk.END,
          "Félicitations, vous avez trouvé le mot de passe !\n Votre score est de "
          + str(self.score), ("center", "large", "yellow"))
      self.root.after(3000, self.end)

  def calculate_distance(self, password1, password2):
    chaine = password1
    password = password2
    distance = 0
    diff = len(password) - len(chaine)
    if diff > 0:
      for i in range(diff):
        chaine += " "
    elif diff < 0:
      for i in range(abs(diff)):
        password += " "
    else:
      pass
    a = 0
    for i in password:
      if str(i) != str(chaine[a]):
        distance += 1
      a += 1
    return distance

  def end(self):
    self.text_area.destroy()
    self.root.quit()

  def close_interface(self, event=None):
    self.text_area.destroy()
    self.root.quit()
